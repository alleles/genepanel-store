# Gene panel: Netthinnesykdommer-v4.0.0 -- File legend
- Netthinne_v4.0.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- Netthinne_v4.0.0_regions.bed:  exonic regions [longest frame fallback strategy]
- Netthinne_v4.0.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- Netthinne_v4.0.0.json:  normalized panel file
