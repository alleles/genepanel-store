# Gene panel: Medfødt hypotyreose-v1.2.0 -- File legend
- Hypotyreose_v1.2.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- Hypotyreose_v1.2.0_regions.bed:  exonic regions [longest frame fallback strategy]
- Hypotyreose_v1.2.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- Hypotyreose_v1.2.0.json:  normalized panel file
