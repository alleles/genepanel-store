# Gene panel: Arvelige nevropatier-v3.0.0 -- File legend
- Nevropati_v3.0.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- Nevropati_v3.0.0_regions.bed:  exonic regions [longest frame fallback strategy]
- Nevropati_v3.0.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- Nevropati_v3.0.0.json:  normalized panel file
