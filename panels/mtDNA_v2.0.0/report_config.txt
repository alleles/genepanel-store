[DEFAULT]
# Used in attachment sent to doctor and internal web
  title=Mitokondrielt DNA
  version=v2.0.0
  coverage_threshold=100
  coverage_description=

[Web publishing - table]
# The values (not the keys) are printed line by line before the gene table.
  legend = [
        ]
