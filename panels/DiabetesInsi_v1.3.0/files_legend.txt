# Gene panel: Diabetes insipidus-v1.3.0 -- File legend
- DiabetesInsi_v1.3.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- DiabetesInsi_v1.3.0_regions.bed:  exonic regions [longest frame fallback strategy]
- DiabetesInsi_v1.3.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- DiabetesInsi_v1.3.0.json:  normalized panel file
