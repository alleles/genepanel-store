# Gene panel: Arvelig bryst- og ovariekreft-v1.1.0 -- File legend
- HBOC_v1.1.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- HBOC_v1.1.0_regions.bed:  exonic regions [longest frame fallback strategy]
- HBOC_v1.1.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- HBOC_v1.1.0.json:  normalized panel file
