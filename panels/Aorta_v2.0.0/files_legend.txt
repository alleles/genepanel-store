# Gene panel: Torakalt aortaaneurisme og aortadisseksjon-v2.0.0 -- File legend
- Aorta_v2.0.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- Aorta_v2.0.0_regions.bed:  exonic regions [longest frame fallback strategy]
- Aorta_v2.0.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- Aorta_v2.0.0.json:  normalized panel file
