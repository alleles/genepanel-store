# Gene panel: Medfødte komplementdefekter-v1.2.0 -- File legend
- Komplement_v1.2.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- Komplement_v1.2.0_regions.bed:  exonic regions [longest frame fallback strategy]
- Komplement_v1.2.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- Komplement_v1.2.0.json:  normalized panel file
