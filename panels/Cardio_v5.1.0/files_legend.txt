# Gene panel: Inherited Cardiac Conditions-v5.1.0 -- File legend
- Cardio_v5.1.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- Cardio_v5.1.0_regions.bed:  exonic regions [longest frame fallback strategy]
- Cardio_v5.1.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- Cardio_v5.1.0.json:  normalized panel file
