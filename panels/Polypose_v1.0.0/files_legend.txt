# Gene panel: Arvelig kreft Polypose-v1.0.0 -- File legend
- Polypose_v1.0.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- Polypose_v1.0.0_regions.bed:  exonic regions [longest frame fallback strategy]
- Polypose_v1.0.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- Polypose_v1.0.0.json:  normalized panel file
