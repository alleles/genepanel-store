# Gene panel: Familiær hyperkolesterolemi-v1.1.0 -- File legend
- HyperChol_v1.1.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- HyperChol_v1.1.0_regions.bed:  exonic regions [longest frame fallback strategy]
- HyperChol_v1.1.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- HyperChol_v1.1.0.json:  normalized panel file
