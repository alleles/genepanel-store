# Gene panel: Arvelig kreft, PMS2-v1.2.0 -- File legend
- PMS2_v1.2.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- PMS2_v1.2.0_regions.bed:  exonic regions [longest frame fallback strategy]
- PMS2_v1.2.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- PMS2_v1.2.0.json:  normalized panel file
