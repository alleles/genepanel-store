# Gene panel: Mikrooftalmi, anoftalmi og kolobom-v2.0.0 -- File legend
- MAC_v2.0.0_phenotypes.tsv:  gene phenotypes/inheritance annotation
- MAC_v2.0.0_regions.bed:  exonic regions [longest frame fallback strategy]
- MAC_v2.0.0_genes_transcripts.tsv:  gene transcripts information [longest frame fallback strategy]
- MAC_v2.0.0.json:  normalized panel file
