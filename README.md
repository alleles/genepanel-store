# Genepanel store

Gene panels created by genepanel-builder

## Update

Panels should be exported from the genepanel builder, and the folders should be put directly under the
`panels` folder. When a tag is created and pushed, the CI will create release artifacts.
