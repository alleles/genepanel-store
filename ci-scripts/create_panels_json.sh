#!/usr/bin/env bash

set -ue

panels_path=${1:-}

if [[ "${panels_path}" == "" ]] ; then
  echo -e "\nusage: $(basename ${BASH_SOURCE[0]}) <panels directory>"
  exit
fi

if [[ ! -d "${panels_path}" ]] ; then
  echo -e "\nno directory called '${panel_path}' found."
  exit
fi

panels_abspath=$(realpath "${panels_path}")
base_abspath=$(dirname "${panels_abspath}")

i=0
echo "{"
while IFS= read -r -d '' d ; do
    panel=$(basename $d)

    regions="${panels_abspath}/${panel}/${panel}_regions.bed"
    transcripts="${panels_abspath}/${panel}/${panel}_genes_transcripts.tsv"
    phenotypes="${panels_abspath}/${panel}/${panel}_phenotypes.tsv"
    report_file="${panels_abspath}/${panel}/report_config.txt"

    test -f $regions || (echo "File not found: $regions" && exit 1)
    test -f $transcripts || (echo "File not found: $transcripts" && exit 1)
    test -f $phenotypes || (echo "File not found: $phenotypes" && exit 1)
    test -f $report_file || (echo "File not found: $report_file" && exit 1)

    i=$((i+1))
    if [ $i -gt 1 ]; then
        echo -ne ","
    fi

    echo -e "\t\"$panel\": {" 
    echo -e "\t\t\"regions\": \"${regions#${base_abspath}/}\","
    echo -e "\t\t\"transcripts\": \"${transcripts#${base_abspath}/}\","
    echo -e "\t\t\"phenotypes\": \"${phenotypes#${base_abspath}/}\","
    echo -e "\t\t\"report_file\": \"${report_file#${base_abspath}/}\""
    echo -e "\t}"
done < <(
  find "${panels_abspath}" -maxdepth 1 -mindepth 1 -type d -print0 | sort -z
)
echo "}"