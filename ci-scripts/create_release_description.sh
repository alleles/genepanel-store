#!/usr/bin/env bash

set -ue

CI_COMMIT_TAG=${CI_COMMIT_TAG:-}

if [[ -z $CI_COMMIT_TAG ]]; then
    COMPARE_TAG=${COMPARE_TAG:-$(git tag --sort version:refname | tail -n 1)}
    DIFF_COMMAND="git diff --no-renames --name-status ${COMPARE_TAG}"
else
    COMPARE_TAG=${COMPARE_TAG-$(git tag --sort version:refname | grep -B 1 ${CI_COMMIT_TAG} | head -n 1)}
    DIFF_COMMAND="git diff --no-renames --name-status ${COMPARE_TAG}..${CI_COMMIT_TAG}"
fi

if [[ "$COMPARE_TAG" == "${CI_COMMIT_TAG}" || ${COMPARE_TAG} == "" ]]; then
    COMPARE_TAG=$(git rev-list --max-parents=0 HEAD) # First commit in repo
    echo -e "## Changes since initial commit\n"
else
    echo -e "## Changes since ${COMPARE_TAG}\n"
fi

CHANGES=$($DIFF_COMMAND | grep -E '^D|^A' | grep -E 'panels/.*_v.*/*.json' | cut -f2 | cut -d '/' -f2 | cut -d '_' -f1 | sort | uniq)

NEW_PANELS=""
DELETED_PANELS=""
UPDATED_PANELS=""

for shortname in $CHANGES; do
    ADDED_VERSION=$($DIFF_COMMAND | grep -E "^A.*panels/${shortname}_v.*/" | grep -oE "v[0-9]+\.[0-9]+\.[0-9]+" | sort -V | tail -n 1)
    DELETED_VERSION=$($DIFF_COMMAND | grep -E "^D.*panels/${shortname}_v.*/" | grep -oE "v[0-9]+\.[0-9]+\.[0-9]+" | sort -V | tail -n 1)

    # If both added version and deleted version exists, then it's an update
    if [ ! -z "$DELETED_VERSION" ] && [ ! -z "$ADDED_VERSION" ]; then
        UPDATED_PANELS="$(cat <(echo -e "$UPDATED_PANELS") <(echo -e "- $shortname $DELETED_VERSION -> $ADDED_VERSION;"))"
    elif [ ! -z "$DELETED_VERSION" ]; then
        DELETED_PANELS="$(cat <(echo -e "$DELETED_PANELS") <(echo -e "- $shortname $DELETED_VERSION;"))"
    else
        NEW_PANELS="$(cat <(echo -e "$NEW_PANELS") <(echo -e "- $shortname $ADDED_VERSION;"))"
    fi

done

echo -e "### New panels\n"
if [ -z "$NEW_PANELS" ]; then
    echo -e "No new panels\n"
else
    echo $NEW_PANELS | sed -E -e 's/;\s?/\n/g'
fi

echo -e "### Updated panels\n"
if [ -z "$UPDATED_PANELS" ]; then
    echo -e "No updated panels\n"
else
    echo $UPDATED_PANELS | sed -E -e 's/;\s?/\n/g'
fi

echo -e "### Deleted panels\n"
if [ -z "$DELETED_PANELS" ]; then
    echo -e "No deleted panels\n"
else
    echo $DELETED_PANELS | sed -E -e 's/;\s?/\n/g'
fi

