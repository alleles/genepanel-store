#!/usr/bin/env bash

set -ue

RELEASE_FILE=$1

# Upload release
function _s3cmd {
    s3cmd \
        --access_key=${DO_SPACES_KEY} \
        --secret_key=${DO_SPACES_SECRET} \
        --host fra1.digitaloceanspaces.com \
        --host-bucket=%\(bucket\)s.fra1.digitaloceanspaces.com \
        $@ > /dev/null
}

_s3cmd put ${RELEASE_FILE} s3://genepanel-store/${CI_COMMIT_TAG}/${RELEASE_FILE}
_s3cmd setacl s3://genepanel-store/${CI_COMMIT_TAG}/${RELEASE_FILE} --acl-public

echo "https://genepanel-store.fra1.digitaloceanspaces.com/${CI_COMMIT_TAG}/${RELEASE_FILE}"
