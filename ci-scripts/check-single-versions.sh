#!/usr/bin/env bash

# 

set -ue

if [[ ! -d "panels" ]]; then
    echo "No panels directory found"
    exit 0
fi
DUP_PANELS=$(ls panels | cut -f1 -d'_' |  sort | uniq -d) 

for panel in $DUP_PANELS; do
    echo "Duplicate panel found:"
    ls panels | grep ${panel}_
done

if [[ ! -z "$DUP_PANELS" ]]; then
    echo "Remove duplicate panels. Only one version per panel is allowed."
    exit 1
fi

