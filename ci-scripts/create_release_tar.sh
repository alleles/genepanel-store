#!/usr/bin/env bash

set -ue

PANELS_JSON=$1

git archive --format=tar.gz --add-file=${PANELS_JSON} ${CI_COMMIT_TAG} .